# frozen_string_literal: true

set :stage, :staging
set :rails_env, :staging
set :branch, :staging
set :deploy_to, '/var/proj/gitlab-test/staging'
server 'dev03.n0p.io', user: 'deployer', roles: %w[app db web]
